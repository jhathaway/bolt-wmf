#!/bin/bash
USAGE=$(
	cat <<-'EOF'
		Small utility to grep out values from a puppet or bolt console output
	EOF
)

set -o errexit
set -o nounset
set -o pipefail

shopt -s lastpipe

function usage {
	cat <<-EOF
		${USAGE}

		usage: ${0} [regexes...]
		  -h This message
		  -p PATH, console output log file, default 'puppet.out'

		Example: $ puppet-out-grep Exim Motd Ferm

	EOF
}

# https://stackoverflow.com/a/17841619/1236063
function join_by {
	local d=${1-} f=${2-}
	if shift 2; then
		printf %s "$f" "${@/#/$d}"
	fi
}

function grep-no-error {
	grep "$@" || [[ $? == 1 ]]
}

function main {
	while getopts ":hp:" opt; do
		case "${opt}" in
		h)
			usage
			return 0
			;;
		p)
			local puppet_out=$OPTARG
			;;
		\?)
			usage 1>&2
			return 1
			;;
		:)
			printf "Option -%s requires an argument\n" "${OPTARG}" >&2
			return 1
			;;
		esac
	done
	shift $((OPTIND - 1))

	if [[ ! -v 'puppet_out' ]]; then
		local repo_root
		repo_root=$(git rev-parse --show-toplevel)
		local puppet_out="${repo_root}/"'puppet.out'
		if [[ ! -e "$puppet_out" ]]; then
			printf 'ERROR: puppet.out not found, %s\n' "$puppet_out"
			return 1
		fi
	fi

	out_ignore=("$@")
	out_ignore+=('Finished' 'Compiled' 'Applied')
	out_ignore_service_regex=$(join_by '|' "${out_ignore[@]}")
	out_ignore+=('refresh')
	out_ignore_regex=$(join_by '|' "${out_ignore[@]}")

	# Grep for everything except Finished, Compiled, Applied, or refresh (and optional arguments)
	filtered=$(grep-no-error -E 'Notice|Error' "${puppet_out}" | grep-no-error -Ev "${out_ignore_regex}")

	# Do a separate grep for only Service refreshes, i.e. restarts, in order to highlight them
	filtered+=$'\n'
	filtered+=$(grep-no-error -Ev "${out_ignore_service_regex}" "${puppet_out}" | grep-no-error --color=always -E 'Service\[.+refresh.*')

	sort <<<"$filtered" | uniq
}

main "$@"
